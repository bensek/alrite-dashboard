<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Center;
use Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->first_name = "John";
        $user->last_name = "Kisakye";
        $user->email = "john@gmail.com";
        $user->phone = "0773343543";
        $user->mobile = "0706820009";
        $user->dob = "20-10-1960";
        $user->sex = "Male";
        $user->marital_status = "Married";
        $user->address = "Ntinda, Kampala";
        $user->password = Hash::make("1234");
        $user->number = "HW-000001";
        $center = Center::first();
        $user->center_id = $center->id;
        $user->save();

    }
}
