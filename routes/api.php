<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\AssessmentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [UserController::class, 'login']);


Route::group(['middleware'=>'auth:api'], function(){
    Route::get('/profile', [UserController::class, 'get_profile']);

    Route::post('/create-patient', [PatientController::class, 'create_patient']);
    Route::get('/get-my-patients', [PatientController::class, 'get_patients_by_user']);
    Route::get('/get-patients-by-center', [PatientController::class, 'get_patients_by_center']);

    Route::post('/create-assessment', [AssessmentController::class, 'create_assessment']);
    Route::get('/get-my-assessments', [AssessmentController::class, 'get_my_assessments']);

});
