<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Center;

class CentersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new Center;
        $c->name = "Buwenge Health Center IV";
        $c->address = "Buwenge, Jinja District";
        $c->number = "HC-001";
        $c->save();

        $c1 = new Center;
        $c1->name = "Mukono Health Center IV";
        $c1->address = "Mukono, Uganda";
        $c1->number = "HC-002";
        $c1->save();

    }
}
