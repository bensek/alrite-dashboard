<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patient;
use Auth;

class PatientController extends Controller
{
    public function create_patient(Request $request){

        $patient = new Patient;
        $patient->surname = $request->surname;
        $patient->given_name = $request->given_name;
        $patient->nickname = $request->nickname;
        $patient->sex = $request->sex;
        $patient->age_range = $request->age_range;
        $patient->weight = $request->weight;
        $patient->user_id = Auth::id();
        $patient->center_id = Auth::user()->center->id;
        $patient->save();
        $patient->number = 'PI-' . str_pad($patient->id, 6, "0", STR_PAD_LEFT);
        $patient->save();

        return response()->json($patient);
    }

    public function get_patients_by_user(){
        $patients = Patient::with('assessments')->where('user_id', Auth::id())->get();
        return response()->json($patients);
    }

    public function get_patients_by_center(){
        $patients = Patient::where('center_id', Auth::user()->center->id)->get();
        return response()->json($patients);
    }
}
