<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Models\User;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'phone' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = request(['phone', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            "code" => 1,
            "message" => "Successfully logged in",
            "data" => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'other_name' => $user->other_name,
                'phone' => $user->phone,
                'email' => $user->email,
                'number' => $user->number,
                'address' => $user->address,
                'sex' => $user->sex,
                'mobile' => $user->mobile,
                'marital_status' => $user->marital_status,
                'center_name' => $user->center->name,
                'center_address' => $user->center->address,
                'role' => $user->role,
                'active' => $user->active,
                'token' => $tokenResult->accessToken,
                'fcm_token' => $user->fcm_token,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]
        ]);
    }

    public function get_profile(){
        $user = User::with('center')->find(Auth::id());

        return response()->json($user);
    }
}
