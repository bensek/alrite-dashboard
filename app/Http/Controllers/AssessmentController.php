<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Assessment;
use Auth;

class AssessmentController extends Controller
{
    public function create_assessment(Request $request){

        $a = new Assessment;
        $a->title = $request->title;
        $a->resp_rate = $request->resp_rate;
        $a->resp_rate_final = $request->resp_rate_final;
        $a->patient_id = $request->patient_id;
        $a->user_id = Auth::id();

        $a->save();

        return response()->json($a);
    }

    public function get_my_assessments(){
        $a = Assessment::with('patient')->where('user_id', Auth::id())->get();

        return response()->json($a);
    }
}
